package network.link;

import network.Signal;
import network.SignalTransceiver;
import network.TransferSwitchable;
import network.node.Node;

public class SingleWayLink extends Link implements SignalTransceiver, TransferSwitchable{

	public SingleWayLink(Node startNode, Node endNode) {
		super(startNode, endNode);
	}

	@Override
	public void transferSignal(Signal s) {
		
	}

	@Override
	public void setTransferable(boolean b) {
		
	}

	@Override
	public boolean isTransferable() {
		return false;
	}

}

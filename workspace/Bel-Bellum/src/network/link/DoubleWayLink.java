package network.link;

import network.Signal;
import network.SignalTransceiver;
import network.node.Node;

public class DoubleWayLink extends Link implements SignalTransceiver{

	public DoubleWayLink(Node startNode, Node endNode) {
		super(startNode, endNode);
	}

	@Override
	public void transferSignal(Signal s) {
		
	}

}

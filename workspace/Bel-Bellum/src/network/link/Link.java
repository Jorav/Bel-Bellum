package network.link;

import network.node.Node;

public abstract class Link {

	protected Node startNode;
	protected Node endNode;
	
	public Link(Node startNode, Node endNode){
		this.startNode = startNode;
		this.endNode = endNode;
	}
	
	/**
	 * Returns an array of Nodes with the starting node as the first element and the ending node as the second.
	 * 
	 * @return Node[] nodes : (startNode, endNode)
	 */
	public Node[] getNodes(){
		Node[] nodes = new Node[2];
		nodes[0] = startNode;
		nodes[1] = endNode;
		return nodes; 
	}
}

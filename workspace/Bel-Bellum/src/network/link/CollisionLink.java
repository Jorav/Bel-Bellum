package network.link;

import network.node.Node;

public class CollisionLink extends DoubleWayLink{

	public CollisionLink(Node startNode, Node endNode) {
		super(startNode, endNode);
	}

}

package network;

public interface TransferSwitchable {
	
	/**
	 * Makes the carrier able/unable to transfer signals depending on boolean b.
	 * @param b
	 */
	public void setTransferable(boolean b);
	
	/**
	 * Returns weather the carrier can transfer a signal or not.
	 * @return boolean
	 */
	public boolean isTransferable();
}

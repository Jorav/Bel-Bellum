package network.node;

import java.util.Stack;

import network.Signal;
import network.SignalTransceiver;
import network.TransferSwitchable;

public class SignalNode extends Node implements SignalTransceiver, TransferSwitchable{

	SignalProcessor signalProcessor;
	Stack<Signal> signals = new Stack<Signal>();
	
	public SignalNode(Object element, SignalProcessor signalProcessor) {
		super(element);
		this.signalProcessor = signalProcessor;
	}

	@Override
	public void transferSignal(Signal s) {
		
	}

	@Override
	public void setTransferable(boolean b) {
		
	}

	@Override
	public boolean isTransferable() {
		return false;
	}
	
	public void resolveSignals(){
		signalProcessor.resolve();
	}
	

}

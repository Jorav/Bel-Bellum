package network.node;

public abstract class Node {
	
	Object element;

	public Node(){}
	public Node(Object element){
		this.element = element;
	}
	
	public Object getElement(){
		return element;
	}
}

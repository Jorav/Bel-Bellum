package network;

public interface SignalTransceiver {
	
	/**
	 * Attempts to transfer the signal.
	 * @param s
	 */
	public void transferSignal(Signal s);
}

package bellum;

public class Modifier {

	protected int permMod = 0;
	protected int tempMod = 0;
	
	public Modifier(){}
	public Modifier(int permanentModifier){
		this.permMod = permanentModifier;
	}
	
	public void addPermModifier(int permanentModifier){
		this.permMod += permanentModifier;
	}
	
	public void addTempModifier(int tempMod){
		this.tempMod += tempMod;
	}

	public int getModifier(){
		return permMod+tempMod;
	}
	
	public void reset(){
		tempMod = 0;
	}
}

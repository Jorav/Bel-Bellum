package bellum;

import network.node.SignalNode;

public class Region extends SignalNode{

	Army garrison;

	public Region(Army garrison) {
		super(garrison);
		this.garrison=garrison;
	}

	public Army getGarrison(){
		return garrison;
	}
	
	public Player getOwner(){
		if(garrison == null)
			return null;
		return garrison.getOwner();
	}
	
	public void resolveConflicts(){
		garrison.fight(armies)
	}

}

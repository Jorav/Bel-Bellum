package bellum;

import java.util.Stack;

public class Army{
	
	 Stack<Unit> armyStack = new Stack<Unit>();
	 Player owner;
	 
	 public Army(Player owner){
		 this.owner = owner;
	 }

	 public void addUnit(Unit unit) {
	  armyStack.push(unit);
	 }

	 public void addArmy(Army army) {
	  while (!army.isEmpty())
	   armyStack.push(army.armyStack.pop());
	 }

	 public boolean isEmpty() {
	  return armyStack.isEmpty();
	 }

	 
	 protected void addTempModifier(int mod){
		 for(Unit unit : armyStack)
			 unit.addTempMod(mod);
	 }

	 public Army fight(Army...armies) {
		 armies = new Army[]();
		 
		 
		 
		 
		 addTempModifier(armies.length*(-1));
		
		return null;
	}
	 
	 public Player getOwner(){
		 return owner;
	 }
}

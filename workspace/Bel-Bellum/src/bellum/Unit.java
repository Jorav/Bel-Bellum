package bellum;

public class Unit{

	protected Modifier mod;
	
	public Unit() {
		mod = new Modifier();
	}
	
	public Unit(int mod){
		this.mod = new Modifier(mod);
	}
	
	public void addTempMod(int mod){
		this.mod.addTempModifier(mod);
	}

	public void addPermMod(int mod){
		this.mod.addPermModifier(mod);
	}

	public Unit fight(Unit...units) {
		// TODO Auto-generated method stub
		return null;
	}
}

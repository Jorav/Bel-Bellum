package bellum;

import java.util.concurrent.ThreadLocalRandom;

public class Die {

	protected int sides;
	
	public Die(int sides) {
		if(sides<1)
			this.sides = 1;
		this.sides = sides;
	}
	
	public int roll(){
		return ThreadLocalRandom.current().nextInt(1, sides+1);
	}

}
